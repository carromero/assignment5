#include <Rcpp.h>
#include <iostream>
using namespace Rcpp;
using namespace std;
// [[Rcpp::export]]

NumericVector simpleBeta1(NumericVector x, NumericVector y){
  int n = 0, size  = x.size();
  double meanX = 0.0, meanY = 0.0,  M2 = 0.0, C2 = 0.0, deltaX, deltaY;
  NumericVector returnVal;
  returnVal = NumericVector(2);
  for(int i = 0; i < size; i++){
    n = n + 1;
    deltaX = x[i] - meanX;
    meanX = meanX + deltaX/n;
    M2 = M2 + deltaX*(x[i] - meanX);
    
    deltaY = y[i] - meanY;
    meanY = meanY + deltaY/n;
    
    C2 = C2 + (y[i]- meanY)*deltaX;
  }
  returnVal[1] = C2/M2;
  returnVal[0] = meanY - returnVal[1]*meanX;
  return returnVal;
  
}
